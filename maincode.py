from scripts.core.function.calculate import calculation
from scripts.core.function.mail import send_test_mail

if __name__ == '__main__':
    max_val, min_val, avg = calculation()
    send_test_mail('nandini.kopparthi@knowledgelens.com,meghana.marpuri@knowledgelens.com')
